package com.nashmaniac.memorableplaces;

import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback, GoogleMap.OnMapLongClickListener{

    private GoogleMap mMap;
    LocationManager locationManager;
    List<String> providerList;
    Location currentLocation;
    int locationInfo = -1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        Intent i = getIntent();
        locationInfo = i.getIntExtra("LocationInfo", -1);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    @Override
    public void onMapLongClick(LatLng latLng) {
        Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
        try{
            List<Address> addresses = geocoder.getFromLocation(latLng.latitude, latLng.longitude,1);
            MainActivity.places.add(addresses.get(0).getAddressLine(0).toString());
            MainActivity.arrayAdapter.notifyDataSetChanged();
            MainActivity.latLangList.add(latLng);
            mMap.addMarker(new MarkerOptions().position(latLng).title(addresses.get(0).getAddressLine(0).toString()));
        }catch (IOException e){
            e.printStackTrace();
        }

    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setOnMapLongClickListener(this);
        if(locationInfo == 0){
            //set to current position
            locationManager =(LocationManager) getSystemService(Context.LOCATION_SERVICE);

            providerList = locationManager.getAllProviders();
            currentLocation=null;
            for(String e: providerList){
                if(currentLocation==null){
                    try{
                        currentLocation = locationManager.getLastKnownLocation(e);
                    }catch (SecurityException et){
                        et.printStackTrace();
                    }

                }else{
                    break;
                }
            }
            mMap.clear();
            LatLng currentPosition = new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude());
            mMap.addMarker(new MarkerOptions().position(currentPosition).title("Current Position"));
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(currentPosition, 12));
        }else{

            mMap.addMarker(new MarkerOptions().position(MainActivity.latLangList.get(locationInfo)).title(MainActivity.places.get(locationInfo)));
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(MainActivity.latLangList.get(locationInfo), 12));
        }

    }
}
